//
//  ViewController.swift
//  UserDefault
//
//  Created by Piyawut on 19/1/2562 BE.
//  Copyright © 2562 Piyawut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var nameTextfield: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    updateName()
  }

  @IBAction func saveNameButtonTouch(_ sender: Any) {
    UserDefaults.standard.set(nameTextfield.text , forKey: "firstName")
    updateName()
  }
  
  fileprivate func updateName() {
    nameLabel.text = UserDefaults.standard.string(forKey: "firstName")
  }
}

